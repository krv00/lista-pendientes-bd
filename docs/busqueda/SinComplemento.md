# Búsqueda sin complemento



  En la barra normal de búsqueda, sin el complemento, Omeka tiene una forma abreviada de búsqueda avanzada:
<p align="center">
     <img src="https://gitlab.com/krv00/lista-pendientes-bd/-/raw/master/docs/img/normal01.png" width="700" />
</p>


En la opción de "**Búsqueda avanzada**" aparece la página con la búsqueda ampliada:

<p align="center">
     <img src="https://gitlab.com/krv00/lista-pendientes-bd/-/raw/master/docs/img/normal02.png" width="700" />
</p>


Y los resultados de las búsqueda tiene este formato:

<p align="center">
     <img src="https://gitlab.com/krv00/lista-pendientes-bd/-/raw/master/docs/img/normal03.png" width="700" />
</p>
