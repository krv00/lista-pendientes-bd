# Opciones de Búsqueda avanzada

 

Omeka tiene dos versiones de búsqueda avanzada. La que se ha evaluado hasta ahora es la que e establece con un complemento especializado.

Las dos versiones de búsquedas avanzadas son estos:

* [Búsqueda avanzada convencional](https://biblioteca-autonoma-discapacidad.gitlab.io/lista-pendientes-bd/busqueda/SinComplemento/)
* [Búsqueda avanzada con complemento](https://biblioteca-autonoma-discapacidad.gitlab.io/lista-pendientes-bd/busqueda/Complemento/)

Como se realizaron observaciones sobre la dificultad de uso de la versión de búsqueda del complemento, hay que elegir una de las dos opciones. En ambos casos, no se puede modificar la forma en cómo están organizadas las categorías y filtros. Crear un nuevo tipo de búsqueda avanzada requeriría un desarrollo de una funcionalidad que no existe en este momento en Omeka (nivel 3).

