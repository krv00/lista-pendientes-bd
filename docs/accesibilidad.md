# Accesibilidad

## Navegación baja visión

### Condiciones variables
Estos cambios requieren de un desarrollo que no está disponible en Omeka y un conocimiento propio de desarrollo web (front) que habría que buscar. 
- [ ] (Nivel 3) **Vista de alto contraste**.
- [ ] (Nivel 3) **Cambio de tamaño de fuente**.
- [ ] (Nivel 3) **Aumento de vista sobre imagen con opción en banco y negro**.

### Condiciones estáticas
- [ ] (Nivel 2) **Tipo de fuente**;  se recomienda Verdana. Sobre este punto, ¿hay que cambiar toda la tipografía a Verdana? ¿Qué tamaño es el adecuado en una vista estática?
- [ ] (Nivel 2) **Mayor contraste** -> ¿Cuánto es el contraste necesario entre texto y fondo? ¿Qué colores son recomendados?

## Navegación personas ciegas

- [X] (Nivel 2) **Descarga en imagen no es útil** -> Agregado en la lista de descripción. Requiere que la url de descarga se agregue cada vez que se sube un elemento.
- [ ] (Nivel 2) **Descripción de imagen** -> La imagen se egenra automáticamente de la primera hoja del PDF, por lo que habrán documento cuya portada sólo sea texto. Se debe revisar si e sposible agregar descripción.
	
## Otros

- [ ] (Nivel 1) ** Sección de Herramientas Digitales**. En la biblioteca se puede crear una sección que puede contener todo tipo de materiales, incluyendo videos, que se pueden consultar, por lo que sólo sería necesario preparar esos material y armar la sección. En la guía de uso se puede señalar el procedimiento para crear una sección nueva.

- [ ] (Nivel 2 ó 3) **Texto Audible** -> Hay que averiguar si e sposible incorporar esto en el código actual o requiere un desarrollo mayor.