# Categorización



## Traducción

- [x] (Nivel 2) La mayoría de los elementos del sitio han sido traducidos.

- [ ] (Nivel 2) Faltan campos de la búsqueda avanzada del complemento que requieren evaluación.

## Identificador

- [x] (Nivel 1) El campo "**Identificador**" puede utilizarse para agregar un identificador único de elemento.

