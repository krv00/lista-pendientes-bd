## Colectivo Disonancia

<p align="center">
     <img src="https://gitlab.com/cdisonancia/graficas_cd/-/raw/master/Logo/Logo_Disonancia_rect%C3%A1ngulo_small.png" width="300" />
</p>

Somos una organización política dedicada a promover Autonomía tecnológica, Autodefensa Digital y crítica del Control Social. Compartimos herramientas y propuestas para la colectivización y la libertad en red 🚩🏴

## Recursos
[Sitio Disonancia](https://colectivodisonancia.net){ .md-button } [:fontawesome-brands-gitlab: GitLab](https://gitlab.com/cdisonancia){: .md-button }